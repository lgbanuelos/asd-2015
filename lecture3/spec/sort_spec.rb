require "sort"

describe "Using blocks" do
	it "should return ['10', '11', '12'] when called with ['10', '11', '12']" do
		input = %w{10 11 12}
		funny_sort(input).should be == input
	end
	it "should return a ['10', '11', '12'] when called with ['10', '12', '11']" do
		input = %w{10 12 11}
		funny_sort(input).should be == input.sort
	end

	it "should return a ['z', 'a', '10', '11', '12'] when called with ['10', '12', '11', 'z', 'a']" do
		letters = %w{z a}
		numbers = %w{10 12 11}
		result = funny_sort(numbers + letters)
		expect(result.first(2)).to match_array letters
		expect(result.last(3)).to eq numbers.sort
	end
	it "should return a ['z', 'a', '-10a', '11x2', '12'] when called with ['-10a', '11x2', '12', 'z', 'a']" do
		letters = %w{z a}
		numbers = %w{-10a 11x2 12}
		result = funny_sort(numbers + letters)
		expect(result.first(2)).to match_array letters
		expect(result.last(3)).to eq numbers
	end
end
