require "group"

describe "Extending a module" do
	describe "Method each_group_by_first_letter" do
		it "should yield ['a', ['abcd', 'axyz', 'able']], ['x', ['xyzab']], ['q', ['qrst']" do
			expect {|b| ['abcd', 'axyz', 'able', 'xyzab', 'qrst'].each_group_by_first_letter(&b) }.to yield_successive_args(['a',['abcd','axyz','able']],['x',['xyzab']],['q',['qrst']])
		end
	end
end